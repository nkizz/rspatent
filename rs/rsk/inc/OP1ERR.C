/* RSK     = 1.1.4    LAST_UPDATE = 1.1.0    */
/***********************************************************************
*
* FILE NAME:    OP1ERR.C
*
* DESCRIPTION:
*
*       Contains the module which defines object processor
*       error messages from the opmakc and opfprocn submodules
*
*     DATE            PROGRAMMER          REASON
* --------------------------------------------------------------------
*     26 AUG. '86      L. Borkow          Original
*      08 SEP. '86     L. Borkow          modifications,null 1st entry
*
/***********************************************************************
*
*                 **** OP1, OP2 ERROR DECLARATIONS *****
*
***********************************************************************/

extern   null_rtn();



ECB  op1_err_table[] =
{

/*null*/"null",null_rtn,
/*OPERR1*/"Heap overflow",null_rtn,                           
/*OPERR2*/"error on scanned object",null_rtn,                            
/*OPERR3*/"this request is not recognized",null_rtn,                     
/*OPERR4*/"no reference call to page template obj",null_rtn,             
/*OPERR5*/"invalid operation for PTO",null_rtn,                          
/*OPERR6*/"no reference call to page template obj",null_rtn,             
/*OPERR7*/"invalid request for window object",null_rtn,                  
/*OPERR8*/"object set not on page",null_rtn,                             
/*OPERR9*/"element object not requested for window",null_rtn,            
/*OPERR10*/"window struct must already be created for page elem",null_rtn,
/*OPERR11*/"unrecognizable encasing object of element object",null_rtn,   
/*OPERR12*/"error return on Obj_seek",null_rtn,                           
/*OPERR13*/"program objects not decoded from scan",null_rtn,              
/*OPERR14*/"no window installed for element call",null_rtn,               
/*OPERR15*/"window pid != element call pid",null_rtn,                     
/*OPERR16*/"element call must occur in page or window object",null_rtn,   
/*OPERR17*/"window structure not found",null_rtn,                         
/*OPERR18*/"error processing partition definition segment",null_rtn,      
/*OPERR19*/"null page default segment",null_rtn,                          
/*OPERR20*/"ascii display stream output not implemented",null_rtn,        
/*OPERR21*/"null presentation data segment",null_rtn,                     
/*OPERR22*/"unrecognizable presentation data type",null_rtn,              
/*OPERR23*/"tbol processor call, invalid offset",null_rtn,                
/*OPERR24*/"selector not in pc segment",null_rtn,                         
/*OPERR25*/"event of program object call not recognizable",null_rtn,      
/*OPERR26*/"can't fetch program object",null_rtn,                         
/*OPERR27*/"bol processor call, invalid offset",null_rtn,                 
/*OPERR28*/"sel/ele call in invalid obj type",null_rtn,                   
/*OPERR29*/"event of field program obj call not recgnzble",null_rtn,      
/*OPERR30*/"no window installed for sel/ele call",null_rtn,               
/*OPERR31*/"window pid != sel/ele call pid",null_rtn,                     
/*OPERR32*/"opmakppt never decodes program data from opscan",null_rtn,    
/*OPERR33*/"error return on Obj_seek",null_rtn,                           
/*OPERR34*/"don't expect page elem struct already created",null_rtn,      
/*OPERR35*/"can't locate field in command bar",null_rtn,                  
/*OPERR36*/"can't locate command bar structure",null_rtn,                 
/*OPERR37*/"failed integer copy from stream",null_rtn,                    
/*OPERR38*/"failed byte copy from stream",null_rtn,                       
/*OPERR39*/"failed string copy from stream",null_rtn,                     
/*OPERR40*/"failed string copy from stream",null_rtn,                     
/*OPERR41*/"PPT window chain error",null_rtn,                             
/*OPERR42*/"PPT window end chain error",null_rtn,                         
/*OPERR43*/"selector returns objectid w/invalid type",null_rtn,           
/*OPERR44*/"requires program object, invalid type",null_rtn,              
/*OPERR45*/"null",null_rtn,                                               
/*OPERR46*/"null",null_rtn,                                               
/*OPERR47*/"null",null_rtn,                                               
/*OPERR48*/"null",null_rtn,                                               
/*OPERR49*/"null",null_rtn,                                               
/*OPERR50*/"invalid operation or object type",null_rtn,                   
