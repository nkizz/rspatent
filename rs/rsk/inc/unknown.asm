;/* RSK     = 1.1.4    LAST_UPDATE = 1.0.0    */
;
; To use malloc() with object caching it's necessary to
; rename it to mallox().  Make OBJ_MGR nonzero and recompile.
;
OBJ_MGR equ 1
