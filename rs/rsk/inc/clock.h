/* RSK     = 1.1.4    LAST_UPDATE = 1.0.0    */
/*
     This file contains definitions for use with clock.c, q.v.
     
     Les Hancock, 4/6/87.
*/
#include <iwssvcsc.h>
#include <iwssvccc.h>
#include <rsystem.h>
#include <rserror.h>

/*
     Important question: why does our clock tick 9.1 times per second?
     Answer: as far as clock.c is concerned, a clock tick is one decrement
     of the timer it creates.  This timer is NOT a TMK timer; it exists in
     the mind of LOS.  LOS handles its timers via timer_task() -- see
     /driver/los/c/rstimers.c.  This task itself is associated with a TMK
     timer set to pop on every second DOS clock tick.  When it pops, it looks
     at all the LOS timers for which it's responsible, decrementing their
     counters and dispatching any that pop.  The DOS clock ticks 18.2 times per
     second (almost exactly) hence the 9.1 figure.
     
     This gives us resolution close to a tenth of a second.
*/
#define TICKS_PER_HOUR 32760        /* clock ticks per hour = 60 * 60 * 9.1 */
#define ALARM 13                                               /* alarm task */
#define CLOCK 12                                               /* clock task */
#define SET_CLOCK   1
#define READ_CLOCK 2
#define TIME_BUFFER_SIZE 6                                   /* for HHMMSS */
#define DATE_BUFFER_SIZE 8                                 /* for MMDDYYYY */
#define STKSIZE     500

/*
   Some definitions that turn up in most LH code.
*/
#define GOOD_ 1
#define NG 0
#define LOOP for(;;)

/*
   Macro to convert 2-char sequence (like "12") to equivalent integer.
   <ptr> contains the address of the 0th character.
*/
#define ATOI_2(ptr) (*(ptr) * 10 + *(ptr + 1) -  11 * '0')

/*
   Macro to validate a time-and-date ICB.  Used only in clock().
*/
#define VALIDATE_TIME(timeptr, days) \
   0 <= timeptr->secs && timeptr->secs <= 59 && \
   0 <= timeptr->mins && timeptr->mins <= 59 && \
   0 <= timeptr->hours && timeptr->hours <= 23 && \
   0 <= timeptr->mon && timeptr->mon <= 11 && \
   0 <= timeptr->mday && timeptr->mday < days[timeptr->mon]

typedef unsigned char BYTE;
typedef unsigned int WORD;
typedef unsigned long int DOUBLEWORD;

/*
   We'll use this template to describe the time.
*/
typedef struct
{
   WORD secs;                                                    /* seconds */
   WORD mins;                                                    /* minutes */
   WORD hours;
   WORD mday;                                       /* day of month, 0 - 30 */
   WORD mon;                                               /* month, 0 - 11 */
   WORD year;                                /* year (full value, viz 1987) */
   WORD is_valid;        /* if 0, clock isn't current; if 1, clock is right */
   WORD was_set;        /* if 0, clock wasn't set; if 1, clock has been set */
} TIME;

/*
   This structure is compatible with system ICB's.
*/
typedef struct {
   BYTE itask;                                         /* task id of sender */
   BYTE itype;                   /* type -- async req/resp or sync req/resp */
   BYTE ifunc;                     /* function to be performed by recipient */
   BYTE istatus;                            /* status of function performed */
   TIME *timeptr; /* ptr to a record that defines the current time and date */
} ICB_TIME;

/*
   Function declarations.
*/
typedef struct
{
   BYTE stack[STKSIZE];
   struct env environ;
} TASK;
