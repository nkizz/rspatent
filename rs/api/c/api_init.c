/* RSK       1.1.4    LAST_UPDATE = 1.0.0    */
/*---------------------------------------------------------------------
**    API_INIT.C       Initialize TBOL interpreter
**--------------------------------------------------------------------
**
**
**    DATE           PROGRAMMER      REASON
*---------------------------------------------------------------------
**  11/05/85        R.G.R.         ORIGINAL
**  09/18/86        R.G.R.         MODIFIED SOUND DEFAULTS
**  16 December '86     Algis           Push pop stack is global to the
                         system.  The stack is an array
                         which is index by a counter.
                         stack_index is initialize to
                         -1 in api_init and is not cleanup
                         in api_cleanup.
**  07/28/87        SLW       Rewrite
**  8/28/87         L.A.      Remove calls for AMPATCH
**  11/10/87        SLW       Free parameter stack on cleanup
**-------------------------------------------------------------------*/

#include <API_INIT.LNT>  /* lint args (from msc /Zg)  */
#include <ERROR.IN>
#include <DEBUG.IN>
#include <malloc.h>
#include <SMDEF.IN>
#include <OBJUNIT.IN>
#include <SMPPT.IN>
#include <INSERR.IN>
#include <RTADEF.IN>
#include <RTASTR.IN>
#include <apityp.IN>
#include <ISCB.IN>
#include <SSCB.IN>
#include <IPCB.IN>
#include <INSCB.IN>
#include <BOOTER.IN>
#include <SLIST.IN>
#include <SYS_TYPE.IN>
#include <ERROR.IN>

#if DEBUG
#include <DBGSTACK.IN>
#endif
/*---------------------------------------------------------------------
**            Externals
**-------------------------------------------------------------------*/
extern unsigned char FAR *ip;
extern RTA rtatbl[];
extern GVAR FAR *glbmid[];
extern LINKLIST FAR *savelist;
extern WINDOW FAR *W;
extern ISCB         *instack;
extern ISCB         instck[ISCB_SIZE + 1];
extern int          stack_index;
extern INSCB        intstck[];
extern IPCB         *pstack;
extern IPCB         parmstck[IPCB_SIZE + 1];
extern SSCB *api_suspend;